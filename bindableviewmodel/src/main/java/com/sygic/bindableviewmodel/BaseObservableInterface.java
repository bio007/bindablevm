package com.sygic.bindableviewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;


interface BaseObservableInterface extends Observable {

    PropertyChangeRegistry getRegistry();

    default PropertyChangeRegistry newRegistryInstance() {
        return new PropertyChangeRegistry();
    }

    @Override
    default void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        getRegistry().add(callback);
    }

    @Override
    default void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        getRegistry().remove(callback);
    }

    /**
     * Notifies listeners that all properties of this instance have changed.
     */
    default void notifyChange() {
        getRegistry().notifyCallbacks(this, 0, null);
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with {@link Bindable} to generate a field in
     * <code>BR</code> to be used as <code>fieldId</code>.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    default void notifyPropertyChanged(int fieldId) {
        getRegistry().notifyCallbacks(this, fieldId, null);
    }
}