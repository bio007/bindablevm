package com.sygic.bindableviewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.PropertyChangeRegistry;

public class BindableViewModel extends ViewModel implements BaseObservableInterface {

    private transient PropertyChangeRegistry callbacks;

    @Override
    public PropertyChangeRegistry getRegistry() {
        synchronized (this) {
            if (callbacks == null) {
                callbacks = newRegistryInstance();
            }
            return callbacks;
        }
    }

}